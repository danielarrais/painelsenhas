package Views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ReciboSenha extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReciboSenha frame = new ReciboSenha();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	JLabel lblSenha = null;
	JLabel lblSenhaGerada = null;
	JLabel lblAtendimento = null;
	JLabel lblTipo = null;
	 
	 
	public ReciboSenha() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 261, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblSenhaGerada = new JLabel("Senha Gerada");
		lblSenhaGerada.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblSenhaGerada.setHorizontalAlignment(SwingConstants.CENTER);
		lblSenhaGerada.setBounds(0, 22, 245, 51);
		contentPane.add(lblSenhaGerada);
		
		lblSenha = new JLabel("");
		lblSenha.setHorizontalAlignment(SwingConstants.CENTER);
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 40));
		lblSenha.setBounds(0, 102, 245, 58);
		contentPane.add(lblSenha);
		
		lblAtendimento = new JLabel("Atendimento");
		lblAtendimento.setHorizontalAlignment(SwingConstants.CENTER);
		lblAtendimento.setBounds(0, 184, 245, 14);
		contentPane.add(lblAtendimento);
		
		lblTipo = new JLabel("");
		lblTipo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTipo.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblTipo.setBounds(0, 202, 245, 24);
		contentPane.add(lblTipo);
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JLabel getLblSenha() {
		return lblSenha;
	}

	public JLabel getLblTipo() {
		return lblTipo;
	}
	
	
}
