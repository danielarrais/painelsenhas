package Views;

import Crontrollers.CtrlAtendimento;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class PainelAtendimento extends JFrame implements Runnable{
	private JButton btnAJ,btnAP,btnAC;
	private JLabel lblEscolha;
	private JPanel contentPane;
	Socket PainelGuiche = null;
	public DataInputStream canalEntrada = null;
	public DataOutputStream canalSaida = null;
	public ReciboSenha senha = new ReciboSenha();
	
	CtrlAtendimento controller = new CtrlAtendimento(this);
	//Metodo utilizado para captura de eventos
	
	public void run()
	{
		while(true)
		{
			try
			{
				String msg = canalEntrada.readUTF();
				String[] dados = msg.split(":");
				dados = dados[1].split(";");
				String senhas= "";
				
				if (dados[1].toCharArray().length==1) {
					senhas = "000" + dados[1];
				}else if (dados[1].toCharArray().length==2) {
					senhas = "00" + dados[1];
				}else if (dados[1].toCharArray().length==3) {
					senhas = "0" + dados[1];
				}
				
				senha.getLblSenha().setText(dados[0]+senhas);
			}catch(Exception e){}
		}
	}
	public static void main(String[] args) {
		new PainelAtendimento();
	}

	public PainelAtendimento() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 334, 497);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(13, 82, 149));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnAP = new JButton("<html><center>Atendimento<br/>Priorit\u00E1rio</center></html>");
		btnAP.setForeground(Color.WHITE);
		btnAP.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnAP.setBackground(new Color(255, 140, 0));
		btnAP.setBounds(51, 226, 215, 77);
		btnAP.addActionListener(controller);
		contentPane.add(btnAP);
		
		btnAJ = new JButton("<html><center>Atendimento<br/>Jur\u00EDdico</center></html>");
		btnAJ.setForeground(Color.WHITE);
		btnAJ.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnAJ.setBackground(new Color(255, 140, 0));
		btnAJ.setBounds(51, 328, 215, 77);
		btnAJ.addActionListener(controller);
		contentPane.add(btnAJ);
		
		btnAC = new JButton("<html><center>Atendimento<br/>Comercial</center></html>");
		btnAC.setBackground(new Color(255, 140, 0));
		btnAC.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnAC.setForeground(Color.WHITE);
		btnAC.setBounds(51, 123, 215, 77);
		btnAC.addActionListener(controller);
		contentPane.add(btnAC);
		
		lblEscolha = new JLabel("<html><center>Selecione o atendimento<br/>desejado</center></html>");
		lblEscolha.setHorizontalAlignment(SwingConstants.CENTER);
		lblEscolha.setForeground(Color.WHITE);
		lblEscolha.setBackground(new Color(13, 82, 149));
		lblEscolha.setFont(new Font("Tahoma", Font.BOLD, 23));
		lblEscolha.setBounds(0, 26, 318, 60);
		contentPane.add(lblEscolha);
		
		try{
			PainelGuiche = new Socket("localhost", 3098);
			canalEntrada = new DataInputStream(PainelGuiche.getInputStream());
			canalSaida = new DataOutputStream(PainelGuiche.getOutputStream());
			new Thread(this).start();
			setTitle("Conectado ao servidor!");
		}
		catch(Exception e){}
		setVisible(true);
	}

	//geters
	public JButton getBtnAJ() {
		return btnAJ;
	}

	public JButton getBtnAP() {
		return btnAP;
	}

	public JButton getBtnAC() {
		return btnAC;
	}
	
	
}
