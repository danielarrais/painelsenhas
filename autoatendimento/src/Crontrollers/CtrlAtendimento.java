package Crontrollers;

import Views.PainelAtendimento;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class CtrlAtendimento implements ActionListener {
	PainelAtendimento painelAtendimento;

	public CtrlAtendimento(PainelAtendimento tela) {
		painelAtendimento = tela;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String msg = "";
		if (e.getSource() == painelAtendimento.getBtnAC()) {
			msg = "C";
			painelAtendimento.senha.getLblTipo().setText("Comercial");
			painelAtendimento.senha.dispose();
			painelAtendimento.senha.setVisible(true);
		} else if (e.getSource() == painelAtendimento.getBtnAP()) {
			msg = "P";
			painelAtendimento.senha.getLblTipo().setText("Preferencial");
			painelAtendimento.senha.dispose();
			painelAtendimento.senha.setVisible(true);
		} else if (e.getSource() == painelAtendimento.getBtnAJ()) {
			msg = "J";
			painelAtendimento.senha.getLblTipo().setText("Judicial");
			painelAtendimento.senha.dispose();
			painelAtendimento.senha.setVisible(true);
		}
		try {
			painelAtendimento.canalSaida.writeUTF("NOVASENHA:" + msg);
			painelAtendimento.canalSaida.flush();
		} catch (IOException e1) {
		    System.out.println("Erro:"+e1.getMessage());
		}
	}
}
