
import Listas.Fila;
import Listas.Pilha;

public class Main {

    public static void main(String[] args) {
        Fila<Integer> minhaFila = new Fila<Integer>();

        Pilha<Integer> minhaPilha = new Pilha<Integer>();

//        minhaFila.add(1);
//        minhaFila.add(2);
//        minhaFila.add(3);
//        minhaFila.add(4);
//        minhaFila.add(5);
//        minhaFila.add(6);
//        minhaFila.verListas.LinkedList();
//        System.out.println(minhaFila.remover());
//        System.out.println(minhaFila.remover());
//        System.out.println(minhaFila.remover());
//        System.out.println(minhaFila.remover());
//        System.out.println(minhaFila.remover());
//        System.out.println(minhaFila.remover());
//
//        System.out.println("=======================================");
//
//        minhaPilha.add(1);
//        minhaPilha.add(2);
//        minhaPilha.add(3);
//        minhaPilha.add(4);
//        minhaPilha.add(5);
//        minhaPilha.add(6);
//        minhaPilha.verListas.LinkedList();
//        System.out.println(minhaPilha.remover());
//        System.out.println(minhaPilha.remover());
//        System.out.println(minhaPilha.remover());
//        System.out.println(minhaPilha.remover());
//        System.out.println(minhaPilha.remover());
//        System.out.println(minhaPilha.remover());

        Listas.LinkedList linkedList = new Listas.LinkedList();

        linkedList.add(0,"OIA0");
        linkedList.add(1,"OIA1");
        linkedList.add(2,"OIA2");
        linkedList.add(3,"OIA3");
        linkedList.add(4,"OIA4");
        linkedList.add(5,"OIA5");

        System.out.print("Buscar:");
        System.out.println(linkedList.busca(5));
        System.out.println(linkedList.size());

        System.out.println("Removido: " + linkedList.remove(0));
        System.out.println("--------------------:");
        System.out.println(linkedList.size());
        linkedList.add(5, "adicionado!");
        System.out.println(linkedList.size());
        linkedList.add(linkedList.size(), "adicionado!1");
     

        System.out.println("--------------------:");
        linkedList.verLinkedList();
        System.out.println(linkedList.size());
    }

}
