package Listas;

public class No<T> {
    private T dado;
    private No proximo;

    public T getDado() {
        return dado;
    }

    public void setDado(T dado) {
        this.dado = dado;
    }

    public No getProximo() {
        return proximo;
    }

    public void setProximo(No proximo) {
        this.proximo = proximo;
    }

    public No(T dado) {
        setDado(dado);
        setProximo(null);
    }
    
    @Override
    public String toString() {
    	return (String) dado+"";
    }
}
