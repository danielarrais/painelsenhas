package Listas;


public class Fila<T> extends Listas.LinkedList<T> {
    public void inserir(T conteudo) {
        super.add(0, conteudo);
    }
    public T remover() {
        return (T) super.remove(super.size()-1);
    }
    public T buscar(){
    	return super.busca(super.size()-1);
    }
}